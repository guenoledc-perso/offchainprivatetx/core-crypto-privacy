'use strict';

const jwt = require('jsonwebtoken');
const rsa = require('node-rsa');
const hash = require('hash.js');
const crypto = require('crypto');
const zlib = require('zlib')
const _ = require('underscore');

const AbstractSharedLedger = require('../ledger/abstract-shared-ledger');
const base64 = require('./base64');


const algos = {
    'aes-128-cbc': {keysize:16, ivsize:16},
    'aes-256-cbc': {keysize:32, ivsize:16}
};
const algo = 'aes-256-cbc';


/*
From a transaction initiator, to create a private transaction with a list of known parties, 
known by their public key it needs
- To create the transaction structure as a JWT
- Adds in it who is the sender by adding its public key
- Sign with the sender's private key to create the base64 encoded token
- calculate the sha256 of the unexcrypted token : hash

- create a random symetric aes key, with its random init vector: {aes-key, init-vector}
- encrypt the token with aes-256-cbc alogorithm : encrypted-token

- push the encrypted-token into the blockchain, indexed with the hash

- while transaction is being integrated in blocks
  - for each participants (including the sender ?) publish a transaction that generates only a log on the block
        - encrypt aes symetric key with participant's public key : encrypted-aes-key
        - encrypt the hash with aes-key : encrypted-hash
        - newEncryptedTransaction(encrypted-aes-key, init-vector, encrypted-hash)

*/
function submitPrivateTransaction(txObject, privateKey, arrayOfPubKeys, ledger, callback) {
    // Should return the hash of the transaction or throw an error otherwise
    // last 2 parameters are asynchronous method implementing blockchain interface (Hexagonal architecture)

    let privkey = null;
    let pubkeys = [];
    let async = (typeof callback ==='function')
    { // check parameters
        if( !txObject ) throw new Error("specify a real object content to be submitted as transaction");
        if( !_.isObject(txObject) ) throw new Error("specify a real object content to be submitted as transaction");

        try {
            privkey = new rsa(privateKey, 'pkcs1-private-pem');
        } catch(failGetPrivateKey) {
            throw new Error("the private key must be a string/buffer in PEM format: "+failGetPrivateKey.message);
        }
        if( privkey == null ) throw new Error("the private key is not properly defined, check you have a provided a valid PKCS1 PEM private key");

        if( !Array.isArray(arrayOfPubKeys) ) throw new Error("specify list of participants as array of public key");
        // de-duplicate the public keys
        arrayOfPubKeys = _.uniq(arrayOfPubKeys)
        arrayOfPubKeys.forEach(pemPubKey => {
            try { pubkeys.push( new rsa(pemPubKey, 'pkcs1-public-pem') ); }
            catch(failGetPublicKey) { throw new Error("the public key provided is not in proper PKCS1 PEM format: "+failGetPublicKey.message); }
        });
        if( !(ledger instanceof AbstractSharedLedger) ) throw new Error("specify a valid ledger object implementing AbstractSharedLedger");
    }
    // restructure object
    let data = zlib.gzipSync(JSON.stringify(txObject))
    console.log('COMPRESSION BENEFIT, DATA', JSON.stringify(txObject).length, data.length)
    
    txObject = {data: data.toString('base64')};
    // set in the tx the public key associated with the private key in bas64 binary format
    txObject.senderPubKey = privkey.exportKey('pkcs1-public-der').toString('base64');
    let pemPrivKey = privkey.exportKey('pkcs1-private-pem');
    let token = jwt.sign(txObject, pemPrivKey, { algorithm: 'RS256', noTimestamp: true });
    let txHash = hash.sha256().update(token);

    const aes_key = crypto.randomBytes(algos[algo].keysize);
    const initVector = crypto.randomBytes(algos[algo].ivsize); 

    txHash = txHash.update(initVector).digest('hex');

    // compress the jwt token so it is smaller during transmissing
    //let compressed_token = zlib.gzipSync(token).toString('base64')
    //console.log('COMPRESSION BENEFIT, TOKEN', token.length, compressed_token.length)
    // symetric encrypt of the jwt 
    let cipher = crypto.createCipheriv(algo,aes_key, initVector);
    let encrypted_token = cipher.update(token, 'utf8', 'base64');
    encrypted_token += cipher.final('base64');
    // symetric encrypt of the tx hash : TODO check why not used
    /*cipher = crypto.createCipheriv(algo, aes_key, initVector);
    let encrypted_hash = cipher.update(txHash, 'utf8', 'base64');
    encrypted_hash += cipher.final('base64');*/

    //let returnStruct = { transactionRecorderResult: null, participantNotificationResult: []};
    let participantKeys = [];
    let to_encrypt = {key: aes_key.toString('hex'), iv: initVector.toString('hex'), hash: txHash};
    pubkeys.forEach(pubkey => {
        participantKeys.push( pubkey.encrypt(to_encrypt, 'base64', 'utf8') );
    });

    try { // implementation can be synchronous (no callback provided) or asynchronous
        let info = {id:txHash, aes_key:aes_key.toString('base64'), iv:initVector.toString('base64')}
        if(async) ledger.transactionRecorder(txHash, encrypted_token, participantKeys, 
            (err, result)=>callback(err, { cryptoInfo:info, ledgerResult:result }));
        else return { cryptoInfo:info, ledgerResult:ledger.transactionRecorder(txHash, encrypted_token, participantKeys) };
    } catch(failRecordingTransaction) {
        throw new Error("Cannot record the private transaction using provided interface: "+failRecordingTransaction.message);
    }

}


/*
From a transaction receiver/participant
- receives a notification of a new transaction via the newEncryptedTransaction event
- decrypt the aes-key with its private key
    - if it fails, then the event is not for this participant, skip event
    - if success, then get the aes-key and the init-vector
- decrypt the hash with aes algorithm : hash

- read the encrypted-token from the public smart contract indexed by hash: encrypted-token
- decrypt de encrypted-token with the aes algorithm, the aes-key and init-vector : token
- decode the token and get the sender public key
- verify the token using the sender's public key to check it has been signed by it
    - if it fails, log an error, it is a possible fraud, need to inform sender's
    - if success, processing of the transaction can be done

*/
// internal only function where parameters have already been checked
function decryptIfForMe(privkey, encrypted_info) {
    // Should return null if the event is not for the privateKey 
    // else return {hash, aes_key, initVector}
    let info = null;
    let initVector = null;
    let aes_key = null;
    try {
        info = privkey.decrypt(encrypted_info, 'json');
        initVector = new Buffer(info.iv, 'hex');
        aes_key = new Buffer(info.key, 'hex');
        return {hash: info.hash, initVector: initVector, aes_key: aes_key};
    } catch( failDecryptWithPrivateKey ) { 
        // this assumed that the provided private key is not suppose to decrypt this key
        return null
    }
}

// internal only function where parameters have already been checked
function decryptToken(encrypted_token, aes_key, initVector) {
    // Should return the decrypted txObject 
    let token = null;
    try {
        let decipher = crypto.createDecipheriv(algo, aes_key, initVector);
        token = decipher.update(encrypted_token, 'base64', 'utf8');
        token += decipher.final('utf8');
    } catch (failDecryptingToken) {
        throw new Error("impossible to decrypt the provided encrypted transaction data. Expected encryption algorithm is "+algo+". Error is "+failDecryptingToken.message);
    }

    // decompress token
    //token = zlib.gunzipSync(Buffer.from(token, 'base64')).toString()

    let signerPubKey = null;
    try {
        let decoded = jwt.decode(token, { algorithms: ['RS512','RS256']});  
        if( _.isUndefined(decoded.senderPubKey) ) throw new Error("the encrypted data is not a valid JWT. It is missing field senderPubKey");
        signerPubKey = new rsa(new Buffer(decoded.senderPubKey, 'base64'), 'pkcs1-public-der');   
    } catch (failDecodingJWT) {
        throw new Error("impossible to decode the decrypted transaction into a JWT with error: "+failDecodingJWT.message);
    }

    try {
        // TODO: need to verify if the signerPubKey is a trusted identity. Or can be done outside this function
        let txObject = jwt.verify(token, signerPubKey.exportKey('pkcs1-public-pem'), { complete:true, algorithms: ['RS512','RS256']}); 
        if( base64.isBase64(txObject.payload.data) ) { // decompress the data
            txObject.data = zlib.gunzipSync( Buffer.from(txObject.payload.data, 'base64') ).toString()
            txObject.data = JSON.parse(txObject.data)
            txObject.jwToken = token
        }
        return txObject;
    } catch (failVerifyJWT) {
        throw new Error("the provided token was not signed by the sender. "+failVerifyJWT.message);
    }

}


function getPrivateTransaction(encrypted_info, privateKey, ledger, onReady) {
    // Should return null if the event is not for the privateKey 
    // Should return the decrypted txObject 
    // transactionReader is a function to get the encrypted tx from its hash
    let async=true; // assume asynchronous calls
    let privkey = null;
    { // check parameters
        try {
            privkey = new rsa(privateKey, 'pkcs1-private-pem');
        } catch(failGetPrivateKey) {
            throw new Error("the private key must be a string/buffer in PEM format: "+failGetPrivateKey.message);
        }

        if( !encrypted_info ) throw new Error("the encrypted info is not provided");
        if( !base64.isBase64(encrypted_info) ) throw new Error("the encrypted info must be a base64 encoded string");

        if( !(ledger instanceof AbstractSharedLedger) ) throw new Error("specify a valid ledger object implementing AbstractSharedLedger");

        if( !_.isFunction(onReady) ) async=false;
    }

    let info = decryptIfForMe(privkey, encrypted_info);
    if( !info ) // Not for my private key
        if( async) { onReady(null, null); return null;}
        else return null; 
    
    return getPrivateTransactionAES(info.hash, info.aes_key, info.initVector, ledger, onReady)
}

function getPrivateTransactionAES(id, aes_key, iv, ledger, onReady) {
    let info ={hash:id, aes_key:aes_key, initVector: iv}
    let async = _.isFunction(onReady)
    let encrypted_token = null;
    try {
        if( async ) // call the given function with a callback
            ledger.transactionReaderAsync(info.hash, (error, encrypted_token) => {
                if(error) onReady(error)
                else try {
                    if( !encrypted_token ) throw new Error("transactionReader provided an invalid data");
                    if( !base64.isBase64(encrypted_token) ) throw new Error("transactionReader must provide a base64 encoded string");
                    let result = decryptToken(encrypted_token, info.aes_key, info.initVector);
                    //result.transactionId = info.hash;
                    result.input = info;
                    onReady(null, result); // calls back with the result
                } catch (failDecryptingToken) {
                    onReady(failDecryptingToken.message, null); // calls back with the error
                }
            }); 
        else {// call is expected to be synchronous - beware, can block the JS thread
           encrypted_token = ledger.transactionReader(info.hash);
           if( !encrypted_token ) throw new Error("transactionReader provided an invalid data");
           if( !base64.isBase64(encrypted_token) ) throw new Error("transactionReader must provide a base64 encoded string");
        }
    } catch (failReadingTheEncryptedTransaction) {
        throw new Error("impossible to retrieve encrypted data for hash "+info.hash+" due to error "+failReadingTheEncryptedTransaction.message);
    }
    if( !async ) { // we were reading the transaction synchronously, now decrypt it
        let result = decryptToken(encrypted_token, info.aes_key, info.initVector);
        //result.transactionId = info.hash;
        result.input = info
        return result;
    }
}

module.exports = { 
    submitPrivateTransaction: submitPrivateTransaction,
    getPrivateTransaction: getPrivateTransaction,
    getPrivateTransactionAES: getPrivateTransactionAES
};