
const notBase64 = /[^A-Z0-9+\/=]/i;

exports.isBase64 = function isBase64(str) {
    if( !(typeof str === 'string')) str = new String(str);
    const len = str.length;
    if (!len || len % 4 !== 0 || notBase64.test(str)) {
        return false;
    }
    const firstPaddingChar = str.indexOf('=');
    return firstPaddingChar === -1 ||
            firstPaddingChar === len - 1 ||
            (firstPaddingChar === len - 2 && str[len - 1] === '=');
}