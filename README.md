# Explanation on how to use
[scheme]: ./documentation/Scheme.png
## Installation
Like any other javascript package (not published on npm yet !)
```sh
    npm i --save core-crypto-privacy
```

## Principles 
The encryption follows the following scheme
![Principle scheme][scheme]
- Sender is the client of the core library that want to send message to several Receivers only known by their RSA public keys
- Receiver is a client of the core library that subscribe to the Ledger for new encrypted event. It must have its own private key
- Ledger is an infrastructure that allow publish and subscribe mechanism and that implements the abstract-shared-ledger interface

