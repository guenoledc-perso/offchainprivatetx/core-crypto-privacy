// Need to understand what is going wrong with this. When using debugger the mocha elements are not in the global variable
if(global.describe === undefined) {
    const mocha = require('mocha')
    global.describe = mocha.describe
    global.it = mocha.it
    global.before = mocha.before
    global.after = mocha.after
}

var chai = require('chai');  
var assert = chai.assert;    // Using Assert style
var expect = chai.expect;    // Using Expect style


const async = require('async');
const fs = require('fs')
const crypto = require('..')
const rsa = require('node-rsa')
const Ledger = require('../ledger/naive-ledger')
const base64 = require('../core/base64')

describe('Privacy crypto unit testing', () => {
    let privatekeys = {} // filename:NodeRsa
    let publickeys = {} // filename:Text
    let messages = [] 
    const ledger = new Ledger()
    after(() => {
        ledger.close()
    });
    before(() => {
        fs.readdirSync(__dirname+'/keys').forEach(f=>{
            const content=fs.readFileSync(__dirname+'/keys/'+f)
            if(content.indexOf('PRIVATE KEY-----')>-1) try{
                privatekeys[f]=new rsa(content, 'pkcs1-private-pem')
                console.log(f,'loaded: format', 'pkcs1-private-pem')
            } catch(e) { try{
                privatekeys[f]=new rsa(content, 'pkcs8-private-pem') 
                console.log(f,'loaded: format', 'pkcs8-private-pem')
            } catch(e){
                console.log(f,'not loaded:', e.message)
            }}
        })
        Object.keys(privatekeys).forEach(f=>publickeys[f]=privatekeys[f].exportKey('pkcs1-public-pem'))
    });
    it('should have the two functions', () => {
        expect(crypto).to.have.property('submitPrivateTransaction')
        expect(crypto).to.have.property('getPrivateTransaction')
    });
    it('naive ledger should give null on historical events', (done) => {
        ledger.loadEvents(null, null, event=>{
            expect(event).to.be.null
            done()
        })
    });
    it('should submit transaction', (done) => {
        const data = {msg:'any json object', value:123}
        const privateKey = Object.values(privatekeys)[0].exportKey('private')
        const pubKeys = Object.values(publickeys)
        crypto.submitPrivateTransaction(data, privateKey, pubKeys, ledger, (err, result)=>{
            expect(err).to.be.null
            expect(result).to.not.be.null
        })
        ledger.onEvent(msg=>{
            //console.log('Event received', msg)
            expect(msg).not.to.be.null
            expect(base64.isBase64(msg)).is.true
            messages.push(msg)
        })
        setTimeout( ()=>{
            done()
        }, 200)
    });

    it('should decrypt message for each private keys each message', (done) => {
        let nbMatch = 0
        async.each(Object.values(privatekeys), (rsaKey, cb)=>{
            async.each(messages, (msg, cb)=>{
                crypto.getPrivateTransaction(msg, rsaKey.exportKey(), ledger, (err, data)=> {
                    expect(err).to.be.null
                    if(data!=null) {
                        expect(data).to.have.property('data')
                        expect(JSON.stringify(data.data)).to.equal(JSON.stringify({msg:'any json object', value:123}))
                        nbMatch++
                    }
                    cb()
                })
            }, cb)
        }, err=>{
            expect(err).to.be.null
            expect(nbMatch).equals(Object.keys(publickeys).length)
            done()
        })
    });
});