# Small reminder to generate compatible keys with openssl

## Generate private key (without encryption)
```sh
openssl genrsa -out keyfile 2048
openssl genrsa -out keyfile 1024
```

## Generate the public key
```sh
openssl rsa -in keyfile -outform PEM -pubout -out public.pub
```

# With ssh-keygen (not compatible format - to be looked at it)

## in new format (not accepted by node-rsa libray)
```sh
ssh-keygen -o -f myKey
```

## in same format as openssh
```sh
ssh-keygen -f myKey
```