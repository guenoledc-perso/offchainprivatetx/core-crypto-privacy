const AsyncEventEmitter = require('async-eventemitter') 

// ledger implementation
class AbstractSharedLedger extends AsyncEventEmitter{
    constructor() {
        super()
        if (this.constructor === AbstractSharedLedger) throw new TypeError("Not implemented");
    }
    transactionRecorder(txid, encrypted_tx, participantsInfos, callback) {
        // callback = function(error, results)
        // if not provided, implementation is expected to be synchronous
        throw new TypeError("Not implemented");
    }
    transactionReader(txid) {
        throw new TypeError("Not implemented");
    }
    transactionReaderAsync(txid, cb) {
        throw new TypeError("Not implemented");
    }
    onEvent(eventCb) {
        if(typeof eventCb === 'function')
            this.on('event', (ev, next)=>{eventCb(ev);next()})
    } 
    loadEvents(from, to, eventCb) {
        throw new TypeError("Not implemented");
    }
    close(cb) {
        throw new TypeError("Not implemented");
    }
}


module.exports = AbstractSharedLedger;