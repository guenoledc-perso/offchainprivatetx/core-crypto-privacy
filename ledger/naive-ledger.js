const AbstractSharedLedger = require('./abstract-shared-ledger');
// In memory ledger implementation
class MemoryLedger extends AbstractSharedLedger {
    constructor() {
        super();
        this.hashmap = {};
        this.events = [];
        this.eventNotifyIndex=0;
        this.counter = 0;
        
        this.timer = setInterval( ()=> {
            while (this.eventNotifyIndex<this.events.length) {
                //console.log("Loop ", this.eventNotifyIndex, this.events[this.eventNotifyIndex]);
                this.emit('event', this.events[this.eventNotifyIndex] );
                this.eventNotifyIndex++;
            }
        }, 200);
    }
    transactionRecorder(txid, encrypted_tx, participantsInfos, callback) {
        this.counter++;
        this.hashmap[txid] = encrypted_tx;
        this.events = this.events.concat(participantsInfos);
        if(typeof callback === 'function') callback(null, this.counter)
        else return this.counter;
    }
    transactionReaderAsync(txid, cb) {
        if( typeof cb === 'function') setTimeout(() => cb(null, this.hashmap[txid]), 100);
        else throw new TypeError('Expecting a callback')
    }
    transactionReader(txid) {
        return this.hashmap[txid];
    }
    loadEvents(from, to, eventCb) {
        //calls eventCb for each events and a final call with null parameter
        eventCb(null)
    }
    close(cb) {
        clearInterval(this.timer);
        cb()
    }
}


module.exports = MemoryLedger;